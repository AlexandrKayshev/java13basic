package professionalmodule.homework2.task2;

import java.util.HashMap;
import java.util.Locale;
import java.util.Scanner;

public class Anagram {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String t = scanner.nextLine();
        System.out.println(anagramOrNot(s,t));
    }

    public static  boolean anagramOrNot(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        HashMap<Character, Integer> s1 = new HashMap<>();
        for (char el : s.toLowerCase(Locale.ROOT).toCharArray()) {
            s1.put(el, s1.getOrDefault(el, 0) + 1);
        }
        HashMap<Character, Integer> s2 = new HashMap<>();
        for (char el : t.toLowerCase(Locale.ROOT).toCharArray()) {
            s2.put(el, s2.getOrDefault(el, 0) + 1);
        }
        return s1.equals(s2);
    }
}
