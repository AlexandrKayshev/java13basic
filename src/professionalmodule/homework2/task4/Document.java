package professionalmodule.homework2.task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Document {
    public int id;
    public String name;
    public int pageCount;

    public Document(int id, String name, int pageCount) {
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }

    public static void main(String[] args) {
        List<Document> list = new ArrayList<>();
        list.add(new Document(1,"SupplyChain", 100));
        list.add(new Document(2,"ClaimFromSupplier", 50));
        list.add(new Document(3,"REP482573958", 40));
        list.add(new Document(4,"QSC-004", 80));

        System.out.println(organizeDocuments(list));
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        HashMap<Integer, Document> result = new HashMap<>();
        for (Document el : documents){
            result.put(el.id, el);
        }
        return result;
    }
    @Override
    public String toString() {
        return "id{" +
                ", name='" + name + '\'' +
                ", pageCount=" + pageCount +
                '}';
    }
}

