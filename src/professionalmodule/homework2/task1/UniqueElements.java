package professionalmodule.homework2.task1;

import java.util.ArrayList;
import java.util.HashSet;

public class UniqueElements {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(4);
        System.out.println(uniqueElements(list));
    }

    public static <T> HashSet<T> uniqueElements(ArrayList<T> from) {
        HashSet<T> result = new HashSet<>();
        result.addAll(from);
        return result;
    }
}
