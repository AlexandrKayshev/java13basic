package professionalmodule.homework3.task2;

import professionalmodule.homework3.task1.IsLike;

public class AnnotationReflection {
    public static void main(String[] args) {
        printClassIsLike(ClassIsLike.class);
    }

    public static void printClassIsLike(Class<?> cls) {
        if (!cls.isAnnotationPresent(IsLike.class)) {
            return;
        }
        IsLike IsLike = cls.getAnnotation(IsLike.class);
        System.out.println("Значение " + IsLike.flag());
    }
}
