package professionalmodule.homework4;

import java.util.List;

public class task3 {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "", "", "def", "qqq");
        long count = list.stream().filter(x -> x.length() > 0).count();
        System.out.println(count);
    }
}
