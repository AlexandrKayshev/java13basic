package professionalmodule.homework4;

import java.util.List;

public class task2 {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2 , 3, 4, 5);
        int sum = list.stream().reduce((a,b) -> a * b).get();
        System.out.println(sum);
    }
}
