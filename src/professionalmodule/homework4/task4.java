package professionalmodule.homework4;

import java.util.Collections;
import java.util.List;

public class task4 {
    public static void main(String[] args) {
        List<Integer> list = List.of(15, -15, 3, -3, 13, 0, 2);
        list.stream().sorted(Collections.reverseOrder()).forEach(System.out::println);
    }
}
