create table products(
                         id_product  serial primary key,
                         title       varchar(50) not null,
                         price       int not null,
                         date_added  timestamp not null
);

insert into products(title, price, date_added)
values ('Розы', '100', now());

insert into products(title, price, date_added)
values ('Лилии', '50', now());

insert into products(title, price, date_added)
values ('Ромашки', '25', now());

create table customers(
                          id_customer     serial primary key,
                          name_customer   varchar(100) not null,
                          phone_number    varchar(100) not null
);

insert into customers(name_customer, phone_number)
values ('Лесков В.И.', '+7-800-555-15-55');

insert into customers(name_customer, phone_number)
values ('Горков П.В.', '+7-800-555-25-55');

insert into customers(name_customer, phone_number)
values ('Океанов Г.Г.', '+7-800-555-35-55');

create table orders(
                       id_order            serial primary key,
                       id_product          int references products,
                       id_customer         int references customers,
                       quantity_of_product int not null check (orders.quantity_of_product >= 1), check ( quantity_of_product <= 1000 ),
                       date_added          timestamp
);

insert into orders(id_product, id_customer, quantity_of_product, date_added)
values ('1', '1', '1000', now());

insert into orders(id_product, id_customer, quantity_of_product, date_added)
values ('2', '2', '31', now() - interval '4d');

insert into orders(id_product, id_customer, quantity_of_product, date_added)
values ('3', '3', '1', now() - interval '40d');

--1. По идентификатору заказа получить данные заказа и данные клиента,
--создавшего этот заказ--
select * from orders t1
join customers t2 on t1.id_customer = t1.id_order
where t1.id_order = 1;

--2. Получить данные всех заказов одного клиента по идентификатору
--клиента за последний месяц
select * from orders
where id_customer = '1' and date_added > current_date - interval '1 months';

--3. Найти заказ с максимальным количеством купленных цветов, вывести их
--название и количество
select t2.title, t1.quantity_of_product max_quatity
from orders t1
inner join products t2 on t1.id_product = t1.id_order
where t1.quantity_of_product = (select max(quantity_of_product) from orders);

--4. Вывести общую выручку (сумму золотых монет по всем заказам) за все
--время
select sum(t2.price * t1.quantity_of_product) as summary_income from orders t1
join products t2 on t1.id_product = cast(t2.id_product as decimal(18,0))









