package professionalmodule.homework1.task3;

import java.io.*;
import java.util.Scanner;

public class ReadWriteFile {
    private static final String FOLDER_DIRECTORY = "C:\\Users\\kay_0\\IdeaProjects\\Java13Basic\\src\\professionalmodule\\task3";
    private static final String OUTPUT_FILE_NAME = "output.txt";

    private ReadWriteFile() {
    }

    public static void ReadWriteFileData(String filePath) throws IOException {
        Scanner sc = new Scanner(new File(filePath));
        Writer writer = new FileWriter(FOLDER_DIRECTORY + "\\" + OUTPUT_FILE_NAME);

        try (sc; writer) {
            String[] arr = new String[10];
            int i = 0;
            while (sc.hasNextLine()) {
                arr[i++] = sc.nextLine();
            }
            for (int j = 0; j < i; j++) {
                String res = arr[j];
                writer.write(res.toUpperCase());
            }
        }
    }
}
