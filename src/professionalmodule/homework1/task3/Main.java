package professionalmodule.homework1.task3;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        String inputFileLocation = "C:\\Users\\kay_0\\IdeaProjects\\Java13Basic\\src\\professionalmodule\\task3\\input.txt";
        try {
            ReadWriteFile.ReadWriteFileData(inputFileLocation);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
