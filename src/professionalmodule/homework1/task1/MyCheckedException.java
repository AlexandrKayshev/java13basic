package professionalmodule.homework1.task1;

public class MyCheckedException extends Exception {
    public MyCheckedException(String message) {
        super(message);
    }
}
