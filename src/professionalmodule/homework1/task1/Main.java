package professionalmodule.homework1.task1;

public class Main {
    public static void main(String[] args) throws MyCheckedException {
        try {
            div(5, 0);
        }
        catch (MyCheckedException e) {
            System.out.println(e.getMessage());
        }
    }
    public static void div(int a, int b) throws MyCheckedException {
        try {
            System.out.println(a / b);
        }
        catch (ArithmeticException e) {
            throw new MyCheckedException("Main#main!error: " + e.getMessage());
        }
        finally {
            System.out.println("Выход из программы");
        }
    }
}