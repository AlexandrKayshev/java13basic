package professionalmodule.homework1.task4;

public class MyPrimeNumber {

    public MyPrimeNumber(int b) throws Exception {
        if (b % 2 != 0) {
            throw new Exception("Нельзя создать инстанс класса MyPrimeNumber с нечетным числом");
        }
    }
}

