package professionalmodule.homework1.task6;

public class CheckGenderException extends Exception{

    public CheckGenderException() {
        super("Неверный пол");
    }
}
