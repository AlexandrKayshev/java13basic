package professionalmodule.homework1.task6;

public class CheckNameException extends Exception {
    public CheckNameException() {
        super("Неверное имя");
    }
}
