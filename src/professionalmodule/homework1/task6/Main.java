package professionalmodule.homework1.task6;

public class Main{
    public static void main(String[] args) {

        try {
            FormValidator.checkName("Alexandr");
        } catch (CheckNameException e) {
            System.out.println(e.getMessage());
        }

        try {
            FormValidator.checkBirthdate("03.05.1998");
        } catch (CheckBirthdateException e) {
            System.out.println(e.getMessage());
        }

        try {
            FormValidator.checkGender("MALE");
        } catch (CheckGenderException e) {
            System.out.println(e.getMessage());
        }

        try {
            FormValidator.checkHeight("194");
        } catch (CheckHeightException e) {
            System.out.println(e.getMessage());
        }
    }
}

