package professionalmodule.homework1.task6;

import java.time.LocalDate;


public class FormValidator {

    public static void checkName(String str) throws CheckNameException {
        if (!str.matches("^[A-Z][a-z]{1,19}$")) {
            throw new CheckNameException();
        }
    }

    public static void checkBirthdate(String str) throws CheckBirthdateException {
        if (!str.matches("(0[1-9]|[12][0-9]|3[01])\\.(0[1-9]|1[012])\\.((19|20)\\d\\d)")) {
            throw new CheckBirthdateException();
        }

        LocalDate localDate = LocalDate.of(Integer.parseInt(str.substring(6)), Integer.parseInt(str.substring(3, 5)),
                Integer.parseInt(str.substring(0, 2)));
        LocalDate toDay = LocalDate.now();

        if (toDay.getYear() < localDate.getYear()) {
            throw new CheckBirthdateException();
        }
        if (toDay.getYear() == localDate.getYear()) {
            if (toDay.getMonthValue() < localDate.getMonthValue()) {
                throw new CheckBirthdateException();
            } else if (toDay.getMonthValue() == localDate.getMonthValue() && toDay.getDayOfMonth() < localDate.getDayOfMonth()) {
                throw new CheckBirthdateException();
            }
        }
    }

    public static void checkGender(String str) throws CheckGenderException {
        try {
            Gender.valueOf(str);
        } catch (Exception e) {
            throw new CheckGenderException();
        }
    }

    public static void checkHeight(String str) throws CheckHeightException {
        try {
            double d = Double.parseDouble(str);
            if (d <= 0) {
                throw new CheckHeightException();
            }
        } catch (Exception e) {
            throw new CheckHeightException();
        }
    }
}

