package professionalmodule.homework1.task6;

public class CheckHeightException extends Exception {

    public CheckHeightException() {
        super("Неверный рост");
    }
}
