package professionalmodule.homework1.task2;

public class Main {
    public static void main(String[] args) {
            div(5, 0);
    }
    public static void div(int a, int b) {
        try {
            System.out.println(a / b);
        }
        catch (ArithmeticException e) {
            throw new MyUncheckedException(e.getMessage());
        }
        finally {
            System.out.println("Выход из программы");
        }
    }
}

