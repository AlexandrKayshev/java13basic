package professionalmodule.homework1.task2;

public class  MyUncheckedException extends ArithmeticException {
    public MyUncheckedException(String message) {
        super(message);
    }
}
