package basicmodule.Homework1_3;

import java.util.Scanner;

public class Solution9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int i;
        for(i = 0; true; i++) {
            int m = sc.nextInt();
            if(m >= 0) {
                break;
            }
        }
        System.out.println(i);
    }
}
