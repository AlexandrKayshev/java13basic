package basicmodule.Homework1_3;

public class Solution1 {
    public static void main(String[] args) {
        int a, b, c;
        for (a = 1; a <= 9; ++a) {
            for (b = 1; b <= 9; b++) {
                c = a * b;
                System.out.println(a + " x " + b + " = " + c);
            }
        }
    }
}
