package basicmodule.Homework1_3;

import java.util.Scanner;

public class Solution6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int res1 = n / 8;
        int res2 = (n % 8) / 4;
        int res3 = (n % 4) / 2;
        int res4 = (n % 2);
        System.out.println(res1 + " " + res2 + " " + res3 + " " + res4);

    }
}
