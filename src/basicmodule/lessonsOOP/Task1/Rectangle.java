package basicmodule.lessonsOOP.Task1;

// Определение класса прямоугольник с двумя конструкторами
class Rectangle {
    double width;
    double height;

    /** Создает  прямоугольник с длиной и шириной, равными 1 */
    Rectangle() {
        width = 1;
        height = 1;
    }

    /** Создает  прямоугольник с указанным значениями */
    Rectangle(double newWidth, double newHeight) {
        width = newWidth;
        height = newHeight;
    }

    /** Возвращает площадь этого  прямоугольник */
    double getArea() {
        return width * height;
    }

    /** Возвращает периметр этого  прямоугольник */
    double getPerimeter() {
        return 2 * (width + height);
    }
}
