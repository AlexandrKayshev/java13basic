package basicmodule.lessonsOOP.Task1;

public class TestRectangle {
    /** Метод main */
    public static void main(String[] args) {
        // Создать прямоугольник (1;1)
        Rectangle rectangle1 = new Rectangle();
        System.out.println("Площадь прямоугольника с шириной "
                + rectangle1.width + " и высотой " + rectangle1.height +  " равна " + rectangle1.getArea());

        // Создать прямоугольник 2 (4;40)
        Rectangle rectangle2 = new Rectangle(4,40);
        System.out.println("Ширина: " +  rectangle2.width + " \nВысота: " + rectangle2.height
        + "\nПлощадь: " + rectangle2.getArea() + "\nПериметр: " + rectangle2.getPerimeter());

        // Создать прямоугольник 3 (3.5;35.9)
        Rectangle rectangle3 = new Rectangle(3.5,35.9);
        System.out.println("Ширина: " +  rectangle3.width + " \nВысота: " + rectangle3.height
                + "\nПлощадь: " + rectangle3.getArea() + "\nПериметр: " + rectangle3.getPerimeter());
    }
}
