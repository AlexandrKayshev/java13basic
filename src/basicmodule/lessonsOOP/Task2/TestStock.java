package basicmodule.lessonsOOP.Task2;

public class TestStock {
    public static void main(String[] args) {
        Stock stock = new Stock("SBER", "ПАО Сбербанк");
        stock.setPreviousClosingPrice(281.50);

        stock.setCurrentPrice(282.87);

        System.out.println(stock.getPreviousClosingPrice());
        System.out.println(stock.getCurrentPrice());
        System.out.println(stock.getChangePercent());
    }
}
