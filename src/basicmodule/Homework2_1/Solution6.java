package basicmodule.Homework2_1;

import java.util.Scanner;

public class Solution6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        morzeCode(s);
    }

    public static void morzeCode(String m) {
        String s = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЦЪЫЬЭЮЯ";
        String[] arr = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.",
                "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};
        for (int i = 0; i < m.length(); i++) {
            for (int j = 0; j < arr.length; j++) {
                if ( m.charAt(i) == s.charAt(j)) {
                    System.out.print(arr[j] + " ");
                }
            }
        }
    }
}