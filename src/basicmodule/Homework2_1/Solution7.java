package basicmodule.Homework2_1;

import java.util.Arrays;
import java.util.Scanner;

public class Solution7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        sum(arr);
    }
    public static void sum(int[] arr) {
        int[] newArr = new int[arr.length];

        int pos = 0;
        for (int element : arr) {
            newArr[pos] = (int) Math.pow(element, 2);
            pos++;
        }
        Arrays.sort(newArr);
        for (int elem : newArr) {
            System.out.print(elem + " ");
        }
    }
}
