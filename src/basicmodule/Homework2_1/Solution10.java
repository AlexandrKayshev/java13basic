package basicmodule.Homework2_1;

import java.util.Random;
import java.util.Scanner;

public class Solution10 {
    public static void main(String[] args) {
        guessTheNumber();
    }

    public static void guessTheNumber() {
        Scanner sc = new Scanner(System.in);
        int number = new Random().nextInt(1000);
        int guessNumber;

        do {
            System.out.println("Введите число от 0 до 1000: ");
            guessNumber = sc.nextInt();

            if (guessNumber == number) {
                System.out.println("Победа!");
                System.exit(0);

            } else if (guessNumber > number) {
                System.out.println("Это число больше загаданного");

            } else if (guessNumber < number) {
                System.out.println("Это число меньше загаданного");
            }
        } while (number != guessNumber);
    }
}
