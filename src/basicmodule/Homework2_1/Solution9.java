package basicmodule.Homework2_1;

import java.util.Scanner;

public class Solution9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String[] strings = new String[n];
        for (int i = 0; i < n; i++) {
            strings[i] = sc.next();
        }
        String s = "";
        for (int i = 0; i < n ; i++) {
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    if (strings[i].equals(strings[j])) {
                        s = strings[i];
                    }
                }
            }
        }
        System.out.println(s);
    }
}
