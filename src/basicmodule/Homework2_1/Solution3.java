package basicmodule.Homework2_1;

import java.util.Arrays;
import java.util.Scanner;

public class Solution3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        Arrays.sort(arr);

        int x = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            if(x < arr[i]) {
                System.out.println(i);
                break;
            }
            if (x >= arr[i] && i == arr.length - 1){
                System.out.println(i + 1);
            }
        }
    }
}

