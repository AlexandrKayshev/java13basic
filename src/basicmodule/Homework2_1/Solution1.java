package basicmodule.Homework2_1;

import java.util.Scanner;

public class Solution1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double[] arr = new double[n];

        for (int i = 0; i < n ; i++) {
            arr[i] = scanner.nextDouble();
        }

        double avarage = 0;
        for (int i = 0; i < n; i++) {
            avarage += arr[i];
        }
        System.out.println(avarage / n);

    }
}
