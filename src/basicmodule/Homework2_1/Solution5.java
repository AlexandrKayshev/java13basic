package basicmodule.Homework2_1;

import java.util.Scanner;

public class Solution5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();

        swapsTheElements(arr,m);

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    public static void swapsTheElements(int[] array, int position) {
        if (position > 0) {
            for (int i = 0; i < position; i++) {
                int n = array[array.length - 1];

                for (int j = array.length - 1; j > 0; j--) {
                    array[j] = array[j - 1];
                }

                array[0] = n;
            }
        } else {
            for (int i = 0; i > position; i--) {
                int n = array[0];

                for (int j = 1; j < array.length; j++) {
                    array[j - 1] = array[j];
                }
                array[array.length - 1] = n;
            }
        }
    }
}
