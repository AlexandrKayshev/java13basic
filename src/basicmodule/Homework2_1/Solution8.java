package basicmodule.Homework2_1;

import java.util.Scanner;

public class Solution8 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int max = 1000;
        int nearest = 0;
        int n = in.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = in.nextInt();
        }
        int m = in.nextInt();
        for (int i = 0; i < n; i++) {
            int x = Math.abs(arr[i] - m);
            if (x <= max) {
                max = x;
                nearest = arr[i];
            }
        }
        System.out.println(nearest);
    }
}

