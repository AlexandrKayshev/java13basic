package basicmodule.Homework2_1;

import java.util.Scanner;

public class Solution4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] arr = new int[n], arrKey = new int[n], arrNumb = new int[n];
        for (int i = 0; i < n; arr[i++] = scanner.nextInt()) ;
        arrKey[0] = arr[0];
        for (int i = 0, j = 0; i < n; i++) {
            if (arr[i] != arrKey[j]) {
                j++;
                arrKey[j] = arr[i];
                arrNumb[j]++;
            } else {
                arrNumb[j]++;
            }
        }
        for (int i = 0; i < arrKey.length; i++) {
            if (arrNumb[i] != 0) {
                System.out.printf("%s %s\n", arrNumb[i], arrKey[i]);
            }
        }
    }
}
