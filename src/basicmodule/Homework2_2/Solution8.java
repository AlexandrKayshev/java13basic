package basicmodule.Homework2_2;

import java.util.Scanner;

public class Solution8 {
    public static void main(String[] arg) {

        Scanner s = new Scanner(System.in);

        int sum = 0;
        int x = s.nextInt();
        int y = recursion(x);
        System.out.println(y);

    }

    public static int recursion(int y) {
        if (y / 10 >= 1) {
            int tempvar = y % 10;
            int remain = y / 10;
            return tempvar + recursion(remain);
        } else {
            return y;
        }

    }
}



