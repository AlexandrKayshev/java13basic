package basicmodule.Homework2_2;

import java.util.Scanner;

public class Solution6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] n = new int[8][4];
        int a = 0, b = 0, c = 0, d = 0;
        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 4; j++) {
                n[i][j] = scanner.nextInt();
            }
        for (int j = 1; j < 8; j++) {
            a += n[j][0];
            b += n[j][1];
            c += n[j][2];
            d += n[j][3];
        }
        if (a <= n[0][0] && b <= n[0][1] && c <= n[0][2] && d <= n[0][3]) {
            System.out.println("Отлично");
        } else {
            System.out.println("Нужно есть поменьше");
        }
    }
}







