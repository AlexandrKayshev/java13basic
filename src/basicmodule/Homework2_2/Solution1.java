package basicmodule.Homework2_2;

import java.util.Scanner;

public class Solution1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        int[][] array = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = scanner.nextInt();
            }
        }
        int min = 1001;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (min > array[i][j]) {
                    min = array[i][j];
                }
            }
            System.out.print(min + " ");
            min = 1001;
        }
    }
}

