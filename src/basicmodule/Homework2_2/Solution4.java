package basicmodule.Homework2_2;

import java.util.Scanner;

public class Solution4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] arr = new int[n][n];
        int res1 = 0;
        int res2 = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = scanner.nextInt();
            }
        }
        int p = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (arr[i][j] == p) {
                    res1 = i;
                    res2 = j;
                }
            }
        }

        for (int i = 0; i < n; i++) {
            if (i != res2) {
                for (int j = 0; j < n; j++) {
                    if (j != res1) {
                        if (j != 0)
                            System.out.print(" ");
                        System.out.print(arr[i][j]);
                    }
                    if (j == n - 1)
                        System.out.print("\n");
                }
            }
        }
    }
}
