package basicmodule.Homework2_2;

import java.util.Scanner;

public class Solution2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] arr = new int[n][n];
        int x1 = scanner.nextInt();
        int y1 = scanner.nextInt();
        int x2 = scanner.nextInt();
        int y2 = scanner.nextInt();

        for (int i = x1; i <= x2; i++) {
            for (int j = y1; j <= y2; j++) {
                if (i > x1 && i < x2 && j > y1 && j < y2) {
                    arr[i][j] = 0;
                }
                else arr[i][j] = 1;
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j < n - 1)
                    System.out.print(arr[j][i] + " ");
                else System.out.print(arr[j][i]);
            }
            System.out.println();
        }
    }
}
