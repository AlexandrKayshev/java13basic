package basicmodule.Homework2_2;

import java.util.Scanner;

public class Solution3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[][] arr = new String[n][n];

        int x1 = scanner.nextInt();
        int y1 = scanner.nextInt();

//        int[][] knightSteps = {{-1, -2}, {-1, 2},
//                {1, -2}, {1, 2},
//                {-2, -1}, {-2, 1},
//                {2, -1}, {2, 1}};

        for (int i = 0; i < n ; i++) {
            for (int j = 0; j < n; j++) {
                if (i == x1 && j == y1) {
                    arr[i][j] = "K";
                } else arr[i][j] = "0";
                if (i == Math.abs(x1 + 1) && j == Math.abs(y1 + 2) || i == Math.abs(x1 - 1) && j == Math.abs(y1 - 2) || i == Math.abs(x1 + 1) && j == Math.abs(y1 - 2)
                         || i == Math.abs(x1 - 1) && j == Math.abs(y1 + 2) || i == Math.abs(x1 - 2) && j == Math.abs(y1 + 1) || i == Math.abs(x1 + 2) && j == Math.abs(y1 - 1)
                        || i == Math.abs(x1 + 2) && j == Math.abs(y1 + 1) || i == Math.abs(x1 - 2) && j == Math.abs(y1 - 1))
                    arr[i][j] = "X";
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j < n - 1)
                    System.out.print(arr[j][i] + " ");
                else System.out.print(arr[j][i]);
            }
            System.out.println();
        }
    }
}
