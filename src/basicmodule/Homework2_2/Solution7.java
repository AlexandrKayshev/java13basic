package basicmodule.Homework2_2;

import java.util.Scanner;

public class Solution7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        String[] name = new String[n];
        for (int i = 0; i < n; i++) {
            name[i] = scanner.next();
        }

        String[] nameOfDog = new String[n];
        for (int i = 0; i < n; i++) {
            nameOfDog[i] = scanner.next();
        }

        int[][] arr = new int[n][3];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < 3; j++) {
                arr[i][j] = scanner.nextInt();
            }

        int a = 0, b = 0, c = 0, d = 0;
        for (int j = 0; j < 3; j++) {
            a += arr[0][j];
            b += arr[1][j];
            c += arr[2][j];
            d += arr[3][j];
        }

        System.out.println(name[3]+ ": " + nameOfDog[3] + ", " + (double)d / 3);
        System.out.println(name[1]+ ": " + nameOfDog[1] + ", " + b / 3 + ".6");
        System.out.println(name[0]+ ": " + nameOfDog[0] + ", " + a / 3 + ".6");
    }
}

