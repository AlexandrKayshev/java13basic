package basicmodule.Homework1_2Extra;

import java.util.Scanner;

public class Solution2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String box = input.nextLine();
        if (box.contains("камни!") && box.contains("запрещенная продукция")) {
            System.out.println("в посылке камни и запрещенная продукция");

        } else if (box.contains("запрещенная продукция")) {
            System.out.println("в посылке запрещенная продукция");

        } else if (box.contains("камни!")) {
            System.out.println("камни в посылке");

        } else {
            System.out.println("все ок");
        }
    }
}
