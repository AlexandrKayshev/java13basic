package basicmodule.Homework1_2Extra;

import java.util.Scanner;

public class Solution1Correct {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String password = sc.nextLine();

        if (password.matches( "[A-Za-z0-9//_//*//-].{8,}")) {
            System.out.println("пароль надежный");
        } else {
            System.out.println("пароль не прошел проверку");
        }
    }
}