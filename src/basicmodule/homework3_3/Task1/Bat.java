package basicmodule.homework3_3.Task1;

public class Bat extends Animal {

    public Bat() {
    }

    @Override
    public void wayOfBirth(){
        System.out.println("Летучая мышь рождается как живородящее");
    }

    @Override
    public void movement() {
        System.out.println("Интересный факт, что летучие мыши могут развивать скорость до 50 км/ч");

    }
}

