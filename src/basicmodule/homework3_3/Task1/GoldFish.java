package basicmodule.homework3_3.Task1;


public class GoldFish extends Animal{
    public GoldFish(){}

    @Override
    public void wayOfBirth(){
        System.out.println("Золотая рыбка мечет икру");
    }

    @Override
    public void movement() {
        System.out.println("Золотая рыбка плывет как и все ее собратья");

    }
}
