package basicmodule.homework3_3.Task1;

public class Main {

    public static void main(String[] args) {

        Bat bat = new Bat();
        bat.eat("Летучая мышь");
        bat.sleep("Летучая мышь");
        bat.wayOfBirth();
        bat.movement();
        System.out.println();

        Dolphin dolphin = new Dolphin();
        dolphin.eat("Дельфин");
        dolphin.sleep("Дельфин");
        dolphin.wayOfBirth();
        dolphin.movement();
        System.out.println();

        GoldFish goldFish = new GoldFish();
        goldFish.eat("Золотая рыбка");
        goldFish.sleep("Золотая рыбка");
        goldFish.wayOfBirth();
        goldFish.movement();
        System.out.println();

        Eagle eagle = new Eagle();
        eagle.eat("Орёл");
        eagle.sleep("Орёл");
        eagle.wayOfBirth();
        eagle.movement();
        System.out.println();
    }
}