package basicmodule.homework3_3.Task1;

public abstract class Animal {

    public final void eat(String str) {
        System.out.println(str + " ест");
    }

    public final void sleep(String str) {
        System.out.println(str + " спит");
    }
    public abstract void wayOfBirth();

    public abstract void movement();
}

