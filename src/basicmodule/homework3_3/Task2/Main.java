package basicmodule.homework3_3.Task2;

public class Main {
    public static void main(String[] args) {

        Thing thing = new Thing();
        Thing thing1 = new Stool();
        Thing thing2 = new Table();

        System.out.println(BestCarpenterEver.stoolRepair(thing));
        System.out.println(BestCarpenterEver.stoolRepair(thing1));
        System.out.println(BestCarpenterEver.stoolRepair(thing2));
        System.out.println();
    }
}
