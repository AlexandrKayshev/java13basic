package basicmodule.Homework3_1.Task6;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AmazingString {
    private char[] chars;

    public AmazingString(char[] chars) {
        this.chars = chars;

    }

    public AmazingString(String str) {

    }

    public char[] getChars() {
        return chars;
    }


    public char returnCharacter(int b) {
        int count = 0;
        char a = 0;
        for (Character el : chars) {
            count++;
            if (count == b) {
                a = el;
            }
        }
        return a;
    }

    public int returnString() {
        int count = 0;
        for (Character el : chars) {
            count++;
        }
        return count;
    }

    public void lineOutput() {
        for (Character el : chars) {
            System.out.print(el + "");
        }
    }

    public boolean substringCharacter(char[] chars1) {
        char a = 0, b = 0;
        int count = 0;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == chars1[0]) {
                for (int c = i, j = 0; j < chars1.length; c++, j++) {
                    a = chars1[j];
                    b = chars[c];
                    if (a == b) {
                        count++;
                    }
                }
            }
        }
        return count >= 1 ? true : false;
    }

    public boolean substringString(String str) {
        Matcher matcher = Pattern.compile(str).matcher(str);
        // не реализовано
        return true;

    }

    public void removeSpaces() {
        int count = 0;
        for (Character el : chars) {
            if (Character.isWhitespace(el)) {
                count++;
            }
        }
        char[] chars1 = new char[chars.length - count];
        for (int i = count - 1; i < chars1.length; i++) {
            chars1 = chars;
            // не реализовано
        }

    }

    public void expandString() {
        for (int i = 0, j = chars.length - 1; i < j; i++, j--) {
            char temp = chars[i];
            chars[i] = chars[j];
            chars[j] = temp;
        }
        System.out.println(chars);
    }
}