package basicmodule.Homework3_1.Task8;

public class ATM {
    private double currency;
    private static int count;

    public ATM(double currency) {
        this.currency = currency;
        count++;
    }

    public double dollars(double dollars) {
        return dollars * currency;
    }

    public double rubles(double rubles) {
        return rubles * currency;
    }

    public static int counter() {
        return count;
    }
}
