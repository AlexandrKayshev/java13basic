package basicmodule.Homework3_1.Task4;

public class TimeUnit {
        private int hour;
        private int minutes;
        private int seconds;

        public TimeUnit(int hour, int minutes, int seconds) {
            this.hour = hour;
            this.minutes = minutes;
            this.seconds = seconds;
        }

        public TimeUnit(int hour, int minutes) {
            this.hour = hour;
            this.minutes = minutes;
        }

        public TimeUnit(int hour) {
            this.hour = hour;
        }

        public void localTime() {
            if (hour <= 24 && hour >= 0 && minutes <= 60 && minutes >= 0 && seconds <= 60 && seconds >= 0) {
                System.out.print(hour < 10 ? "0" + hour + ":" : hour + ":");
                System.out.print(minutes < 10 ? "0" + minutes + ":" : minutes + ":");
                System.out.println(seconds < 10 ? "0" + seconds : seconds);
            } else {
                System.out.println("Вы указали не корректное время");
            }
        }
        public void localTimeTwelve() {
            if (hour <= 24 && hour >= 0 && minutes <= 60 && minutes >= 0 && seconds <= 60 && seconds >= 0) {
                if (hour < 12) {
                    System.out.print(hour < 10 ? "0" + hour + ":" : hour + ":");
                    System.out.print(minutes < 10 ? "0" + minutes + ":" : minutes + ":");
                    System.out.print(seconds < 10 ? "0" + seconds : seconds);
                    System.out.println(" am");
                } else if (hour <= 24) {
                    int a = hour - 12;
                    System.out.print(a < 10 ? "0" + a + ":" : a + ":");
                    System.out.print(minutes < 10 ? "0" + minutes + ":" : minutes + ":");
                    System.out.print(seconds < 10 ? "0" + seconds : seconds);
                    System.out.println(" pm");
                }
            } else {
                System.out.println("Вы указали не корректное время");
            }
        }

        public void localTimePlus(int a, int b, int c) {
            this.hour += a;
            this.minutes += b;
            this.seconds += c;
        }
    }
