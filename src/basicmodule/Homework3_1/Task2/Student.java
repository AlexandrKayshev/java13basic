package basicmodule.Homework3_1.Task2;

public class Student {
    private double sum = 0;
    private String name;
    private String surname;
    private int[] grades;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    public int[] getGrades() {
        return grades;
    }

    public void newGrades() {
        System.arraycopy(grades, 1, grades, 0, grades.length - 1);
        grades[grades.length - 1] = 3;
    }

    public double averageMark() {
        for (int res : grades) {
            sum += res;
        }
        return (int) (sum / grades.length * 10) / 10.0;
    }
}

