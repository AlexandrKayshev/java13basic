package basicmodule.Homework3_1.Task3;

import basicmodule.Homework3_1.Task2.Student;

public class StudentService {
    Student student = new Student();

    public Student bestStudent(Student[] students) {
        Student max = students[0];
        for (int i = 1; i < students.length; i++) {
            if (students[i].averageMark() > max.averageMark()) {
                max = students[i];
            }
        }
        return max;
    }

    public void sortBySurname(Student[] students) {
        int a;
        Student temp;
        for (int i = 0; i < students.length; i++) {
            for (int j = 0; j < students.length - i - 1; j++) {
                a = students[j].getSurname().compareTo(students[j + 1].getSurname());
                if (a > 0) {
                    temp = students[j];
                    students[j] = students[j + 1];
                    students[j] = temp;
                }
            }
        }
    }
}

