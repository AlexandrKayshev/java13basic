package basicmodule.Homework3_1.Task5;

public class DayOfWeek {
    private byte day;
    private String dayOfWeek;

    public DayOfWeek(byte day, String dayOfWeek) {
        this.day = day;
        this.dayOfWeek = dayOfWeek;
    }

    public byte getDay() {
        return day;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }
}

