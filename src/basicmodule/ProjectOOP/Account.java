package basicmodule.ProjectOOP;

import java.util.ArrayList;
import java.util.Date;

public class Account {
    private int id;
    private double balance;
    private static double annualInterestRate;
    private Date dateCreated;
    private String name;
    private ArrayList<Transaction> transactions = new ArrayList<>();

    public Account() {
        dateCreated = new Date();
    }

    public Account(int id, double balance) {
        this.id = id;
        this.balance = balance;
        dateCreated = new Date();
    }

    public Account(String name, int id, double balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        dateCreated = new java.util.Date();
    }

    /** Возвращает id */
    public int getId() {
        return id;
    }

    /** Возвращает баланс */
    public double getBalance() {
        return balance;
    }

    /** Возвращает годовую процентную ставку */
    public static double getAnnualInterestRate() {
        return annualInterestRate;
    }

    /** Возвращает дату создания счета */
    public Date getDateCreated() {
        return dateCreated;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    /** Присваивает новый id */
    public void setId(int id) {
        this.id = id;
    }

    /** Присваивает новый баланс */
    public void setBalance(double balance) {
        this.balance = balance;
    }

    /** Присваивает новую годовую процентную ставку */
    public static void setAnnualInterestRate(double annualInterestRate) {
        Account.annualInterestRate = annualInterestRate;
    }

    public double getMonthlyInterest() {
        return balance * (annualInterestRate / 1200);
    }

    public void withdraw(double amount) {
        balance -= amount;
        transactions.add(new Transaction('-', amount, balance, ""));
    }

    public void deposit(double amount) {
        balance += amount;
        transactions.add(new Transaction('+', amount, balance, ""));
    }
}
