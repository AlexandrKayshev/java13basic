package basicmodule.homework3_2;

public class Main {

    public static void main(String[] args) {
        Library library = new Library();
        library.book.nameBook(); // Получаем имеющееся книги в библиотеке

        Book book1 = new Book("Сильмариллион", "Дж.Р.Р.Толкин");
        Book book2 = new Book("Мастер и Маргарита", "М.Булгаков");
        Book book3 = new Book("Берсерк", "К.Миура");


        Visitor visitor = new Visitor("Александр");
        library.visitorArrays(visitor);
        System.out.println("Имя читателя: " + visitor.getVisitorName() + "\n" + "ID-пользователя: " + visitor.getUserId());

        library.addBook(book1); // проверка метода добавление книги
        library.addBook(book2); // проверка метода добавления книги если есть такая книга


        library.returnBooks(book2); // находим и возвращаем книгу по названию

        library.deleteBook(book2); // удаляем книгу по названию

        library.returnAuthor("Х.Мураками"); // Вернуть список книг по автору

        library.takeBook(visitor, book1); // Механиз одалживания книги

        library.returnBookVisitor(visitor,book1); // метод возвращения книги
        System.out.println(book1.getVisitor() + "  " + visitor.getBook());

        // проверка вывода средней арифметической оценки
        System.out.println(library.bookRating(visitor,book1));
        System.out.println(library.bookRating(visitor,book1));

        System.out.println(library.bookRating(visitor,book3));
        System.out.println(library.bookRating(visitor,book3));

    }
}

