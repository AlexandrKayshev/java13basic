package basicmodule.homework3_2;

import java.util.ArrayList;
import java.util.List;

public class Book {
    private String nameOfTheBook;
    private String authorTitle;
    private Visitor visitor = null;
    private double result = 0.0;
    private int grade;

    ArrayList<Integer> scoreArray = new ArrayList<>();


    public Visitor getVisitor() {
        return visitor;
    }

    public void setVisitor(Visitor visitor) {
        this.visitor = visitor;
    }

    public Book() {
    }

    public Book(String nameOfTheBook, String authorTitle) {
        this.nameOfTheBook = nameOfTheBook;
        this.authorTitle = authorTitle;
    }

    public String getNameOfTheBook() {
        return nameOfTheBook;
    }

    public String getAuthorTitle() {
        return authorTitle;
    }

    public void setNameOfTheBook(String nameOfTheBook) {
        this.nameOfTheBook = nameOfTheBook;
    }

    public void setAuthorTitle(String authorTitle) {
        this.authorTitle = authorTitle;
    }

    List<Book> bookArrays = new ArrayList<>();

    public void nameBook() {
        bookArrays.add(new Book("Кафка на пляже", "Х.Мураками"));
        bookArrays.add(new Book("Мастер и Маргарита", "М.Булгаков"));
        bookArrays.add(new Book("Хроники заводной птицы", "Х.Мураками"));
        bookArrays.add(new Book("Берсерк", "К.Миура"));
        bookArrays.add(new Book("Под куполом", "С.Кинг"));
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getGrade() {
        return grade;
    }
}





