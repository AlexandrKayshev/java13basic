package basicmodule.homework3_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Library {

    public Library() {
    }

    Visitor visitor = new Visitor();

    Book book = new Book();

    public void addBook(Book bookName) {
        for (Book el : book.bookArrays) {
            if (bookName.getNameOfTheBook().equals(el.getNameOfTheBook()) &&
                    bookName.getAuthorTitle().equals(el.getAuthorTitle())) {
                System.out.println("Книга не может быть добавлена. Эта книга есть в библиотеке");
                return;
            }
        }
        book.bookArrays.add(bookName);
    }

    public void visitorArrays(Visitor visitorName) {
        visitor.generateId();
        visitor.visitorArray.add(visitor);
    }

    public void deleteBook(Book bookName) {
        boolean a = false, b = false;
        for (Book el : book.bookArrays) {
            if (bookName.getNameOfTheBook().equals(el.getNameOfTheBook()) &&
                    bookName.getAuthorTitle().equals(el.getAuthorTitle())) {
                a = true;
                if (bookName.getVisitor() == null) {
                    b = true;
                }
            }
        }
        if (a && b) {
            book.bookArrays.remove(bookName);
            System.out.printf("Книга %s удалена" + "\n", bookName.getNameOfTheBook());
        } else if (!a) {
            System.out.println("Такой книги нет в библиотеке");
        } else {
            System.out.println("Книга находится у читателя");
        }
    }

    public Book returnBooks(Book bookName) {
        for (Book el : book.bookArrays) {
            if (bookName.getNameOfTheBook().equals(el.getNameOfTheBook())) {
                System.out.println(el.getNameOfTheBook() + " " + el.getAuthorTitle());
                return book;
            }
        }
        System.out.println("В библиотеке нет такой книги");
        return bookName;
    }

    public List<Book> returnAuthor(String bookAuthor) {
        List<Book> bookAut = new ArrayList<>();
        for (Book el : book.bookArrays) {
            if (bookAuthor.equals(el.getAuthorTitle())) {
                bookAut.add(el);
                System.out.println(el.getNameOfTheBook() + " " + el.getAuthorTitle());
            }
        }
        return bookAut;
    }

    public void takeBook(Visitor visitorId, Book bookName) {
        boolean a = false, b = false, c = false;
        for (Book el : book.bookArrays) {
            if (bookName.getNameOfTheBook().equals(el.getNameOfTheBook()) &&
                    bookName.getAuthorTitle().equals(el.getAuthorTitle())) {
                a = true;
            }
        }

        for (Visitor el1 : visitor.visitorArray) {
            if (visitorId.getUserId() != 0 && visitorId.getBook() == null) {
                b = true;
            }
        }
        for (Book el2 : book.bookArrays) {
            if (bookName.getVisitor() == null) {
                c = true;
            }
        }
        if (a && b && c) {
            visitorId.setBook(bookName);
            bookName.setVisitor(visitorId);
            System.out.println(bookName.getNameOfTheBook() + " выдана " + bookName.getVisitor().getVisitorName());
        } else if (!a) {
            System.out.println("Такой книги нет в библиотеке");
        } else if (!b) {
            System.out.println("Книга находится у посетителя");
        } else {
            System.out.println("Книга в данный момент одолжена");
        }
    }

    public void returnBookVisitor(Visitor visitorName, Book bookName) {
        for (Book el : book.bookArrays) {
            if (bookName.getNameOfTheBook().equals(el.getNameOfTheBook())) {
                bookName.setVisitor(null);
            }
        }
        for (Visitor el : visitor.visitorArray) {
            if (visitorName.getUserId() == el.getUserId()) {
                visitorName.setBook(null);
            }
        }
    }

    public double bookRating(Visitor visitorName, Book bookName) {
        Scanner in = new Scanner(System.in);
        for (Book el : book.bookArrays) {
            if (bookName.getNameOfTheBook().equals(el.getNameOfTheBook())) {
                System.out.println(visitorName.getVisitorName() + " поставьте оценку прочитанной книге " + bookName.getNameOfTheBook());
                bookName.setGrade(in.nextInt());
                bookName.scoreArray.add(bookName.getGrade());
            }
        }
        double sum = 0.0;
        for (int i = 0; i < bookName.scoreArray.size(); i++) {
            sum += bookName.scoreArray.get(i);
        }
        bookName.setResult((int) ((sum / bookName.scoreArray.size()) * 10) / 10.0);
        return bookName.getResult();
    }
}


