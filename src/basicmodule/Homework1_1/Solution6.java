package basicmodule.Homework1_1;

import java.util.Scanner;

public class Solution6 { static final double KM_IN_MILLE = 1.60934;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double km = sc.nextDouble();
        double mille = km / KM_IN_MILLE;
        System.out.println(mille);
    }
}
