package basicmodule.Homework1_1;

import java.util.Scanner;

public class Solution5 {
    static final double CENTIMETERS_IN_INCH = 2.54;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double n = sc.nextDouble();
        double res = n * CENTIMETERS_IN_INCH;
        System.out.println(res);

    }
}
