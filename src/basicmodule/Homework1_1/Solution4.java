package basicmodule.Homework1_1;

import java.util.Scanner;

public class Solution4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int sec = sc.nextInt();
        int min1 = sec / 60;
        int hours = min1 / 60;
        int min2 = (sec / 60) - hours * 60;

        System.out.println(hours + " " + min2);
    }
}