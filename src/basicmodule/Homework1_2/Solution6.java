package basicmodule.Homework1_2;

import java.util.Scanner;

public class Solution6 {

    public static final int BEGIN = 500;
    public static final int PRE = 1500;
    public static final int INT = 2500;
    public static final int UPPER = 3500;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int qtyWords = sc.nextInt();
        if (qtyWords < BEGIN) {
            System.out.println("beginner");

        } else if (qtyWords >= BEGIN && qtyWords < PRE) {
            System.out.println("pre-intermediate");

        } else if (qtyWords >= PRE && qtyWords < INT) {
            System.out.println("intermediate");

        } else if (qtyWords >= INT && qtyWords < UPPER) {
            System.out.println("upper-intermediate");

        } else if (qtyWords >= UPPER) {
            System.out.println("fluent");
        }
    }
}
