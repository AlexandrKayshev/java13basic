package basicmodule.Homework1_2;

import java.util.Scanner;

public class Solution4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int res = 6 - n;
        if (n >= 6) {
            System.out.println("Ура, выходные!");
        } else {
            System.out.println(res);
        }
    }
}
