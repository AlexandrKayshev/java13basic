package basicmodule.Homework1_2;

import java.util.Scanner;

public class Solution8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        int k = s.lastIndexOf(' ');
        String firstWord = s.substring(0, k);
        String secondWord = s.substring(k + 1);
        System.out.println(firstWord + "\n" + secondWord);
    }
}