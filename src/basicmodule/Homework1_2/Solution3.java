package basicmodule.Homework1_2;

import java.util.Scanner;

public class Solution3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        if ( x <= 12) {
            System.out.println("Рано");
        } else {
            System.out.println("Пора");
        }
    }
}