package basicmodule.Homework1_2;

import java.util.Scanner;

public class Solution9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        System.out.println( Math.pow(Math.sin(Math.toRadians(x)),2) + Math.pow(Math.cos(Math.toRadians(x)),2) - 1 == 0 );

    }
}
