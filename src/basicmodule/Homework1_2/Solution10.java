package basicmodule.Homework1_2;

import java.util.Scanner;

public class Solution10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double n = sc.nextDouble();
        System.out.println(Math.log(Math.pow(Math.E,n)) == n);
    }
}
